import numpy as np

class SimpleNN:


    def __init__(self, n_inputs, n_hidden, n_outputs, weights_matrix_1, weights_matrix_2):
        """
        SimpleNN algorithm implementation. Consists of forward function.
        :param n_inputs int: number of input features.
        :param n_hidden int: number of hidden nodes.
        :param n_outputs int: number of output (y's shape)
        :param weights_matrix_1 ndarray: the weights between input layer and hidden layer, dimensions n_inputsxn_hidden
        :param weights_matrix_2 ndarray: the weights between hidden layer layer and output layer, dimensions n_hiddenxn_outputs
        """
        self.n_inputs = n_inputs
        self.n_hidden = n_hidden
        self.n_outputs = n_outputs
        self.weights_matrix_1 = weights_matrix_1
        self.weights_matrix_2 = weights_matrix_2

    def check_if_plausible(self):
        if self.n_inputs == self.weights_matrix_1.shape[0] and \
                self.weights_matrix_1.shape[1] == self.n_hidden and \
                self.n_hidden == self.weights_matrix_2.shape[0] and \
                self.weights_matrix_2.shape[1] == self.n_outputs:
            return True
        else:
            return False

    def forward(self, x):
        """
        Perform a forward pass, input dimensions should be as declared in constructor
        :param x ndarray: of the input of input features.
        Returns:
        ndarray: numpy array that represents the output
        """
        hidden = self.calc_hidden_layer(x)
        output = np.matmul(hidden, self.weights_matrix_2)
        return output

    def calc_hidden_layer(self,x):
        return np.matmul(x, self.weights_matrix_1)

