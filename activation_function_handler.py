import numpy as np

class ActivationFunctionHandler:

   def __init__(self):
       self.function_name_mappings = {'relu': self.relu,
                                        'softmax': self.softmax,
                                        'sigmoid': self.sigmoid,
                                        'identity': self.identity}

   def calc_activation_values(self,func_name,x):
       """
       :param func_name string: needs to be the name of 1 of the supported functions
       :param x ndarray: the values we want to calculate their activation function result
       :return: ndarray: values transformed by activation function
       """
       return self.function_name_mappings[func_name](x)

   def softmax(self,x):
       """
       :param x ndarray: the values we want to calculate their activation function result
       :return: ndarray:  values transformed by softmax function
       """
       pass

   def relu(self,x):
       """
       :param x ndarray: the values we want to calculate their activation function result
       :return: ndarray: values transformed by relu function
       """
       x[x < 0] = 0
       return x

   def sigmoid(self,x):
       """
       :param x ndarray: the values we want to calculate their activation function result
       :return: ndarray: values transformed by sigmoid function
       """
       return 1 / (1 + np.exp(-x))

   def identity(self,x):
       """
       :param x ndarray: the values we want to calculate their activation function result
       :return: ndarray: values transformed by identity function
       """
       return x

