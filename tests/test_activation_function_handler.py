import pytest
import numpy as np
from activation_function_handler import ActivationFunctionHandler


def test_identity_1_4():
    x = np.asarray([1,4])
    activation_handler = ActivationFunctionHandler()
    output = activation_handler.identity(x)
    assert (output == x).all() == True

def test_identity_0():
    x = np.asarray([0])
    activation_handler = ActivationFunctionHandler()
    output = activation_handler.identity(x)
    assert (output == x).all() == True

def test_identity_1_4_78_6():
    x = np.asarray([-1,4,7.8,6])
    activation_handler = ActivationFunctionHandler()
    output = activation_handler.identity(x)
    assert (output == x).all() == True

def test_relu_1_4():
    x = np.asarray([1, 4])
    activation_handler = ActivationFunctionHandler()
    output =  activation_handler.relu(x)
    assert (output == np.asarray([1, 4])).all() == True

def test_relu__1__4():
    x = np.asarray([-1, -4])
    activation_handler = ActivationFunctionHandler()
    output =  activation_handler.relu(x)
    assert (output == np.asarray([0, 0])).all() == True

def test_relu__1__4_8_16():
    x = np.asarray([-1, -4, 8, 16])
    activation_handler = ActivationFunctionHandler()
    output =  activation_handler.relu(x)
    assert (output == np.asarray([0, 0,8,16])).all() == True

def test_sigmoid_1_4():
    x = np.asarray([1, 4])
    activation_handler = ActivationFunctionHandler()
    output = activation_handler.sigmoid(x)
    assert (output == np.asarray([0, 0,8,16])).all() == True

def test_calc_activation_values_relu__1_4():
    x = np.asarray([-1, 4])
    activation_handler = ActivationFunctionHandler()
    output = activation_handler.calc_activation_values("relu",x)
    assert (output == np.asarray([0, 4])).all() == True

def test_calc_activation_values_identity__1_4():
    x = np.asarray([-1, 4])
    activation_handler = ActivationFunctionHandler()
    output = activation_handler.calc_activation_values("identity",x)
    assert (output == np.asarray([-1, 4])).all() == True


if __name__=='__main__':
    pytest.main()