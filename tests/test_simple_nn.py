import pytest
import numpy as np
from simple_nn import SimpleNN


weights_1 = np.asarray([[0.5,1.5,2],[1,2,3]])
weights_2 = np.asarray([[1,0.5],[1,0.2],[2,0.2]])



def test_check_if_plausible_returns_false():
    weights_1 = np.asarray([[0.1,2]])
    weights_2 = np.asarray([[1,5,6]])
    simple_nn = SimpleNN(3, 3, 2, weights_1, weights_2)
    assert simple_nn.check_if_plausible() == False

def test_check_if_plausible_returns_true():
    weights_1 = np.asarray([[0.5, 1.5, 2], [1, 2, 3]])
    weights_2 = np.asarray([[1, 0.5], [1, 0.2], [2, 0.2]])
    simple_nn = SimpleNN(2, 3, 2, weights_1, weights_2)
    assert simple_nn.check_if_plausible() == True

def test_calc_hidden_layer_1_4():
    weights_1 = np.asarray([[0.5, 1.5, 2], [1, 2, 3]])
    weights_2 = np.asarray([[1, 0.5], [1, 0.2], [2, 0.2]])
    input_x = np.asarray([1,4])
    simple_nn = SimpleNN(2, 3, 2, weights_1, weights_2)
    output = simple_nn.calc_hidden_layer(input_x)
    assert (output == np.asarray([4.5, 9.5,14])).all() == True

def test_forward_input_1_4():
    weights_1 = np.asarray([[0.5, 1.5, 2], [1, 2, 3]])
    weights_2 = np.asarray([[1, 0.5], [1, 0.2], [2, 0.2]])
    input_x = np.asarray([1,4])
    simple_nn = SimpleNN(2, 3, 2, weights_1, weights_2)
    output = simple_nn.forward(input_x)
    assert (output == np.asarray([42.0, 6.95])).all() == True

def test_calc_hidden_layer_2_6():
    weights_1 = np.asarray([[0.5, 1.5, 2], [1, 2, 3]])
    weights_2 = np.asarray([[1, 0.5], [1, 0.2], [2, 0.2]])
    input_x = np.asarray([2,6])
    simple_nn = SimpleNN(2, 3, 2, weights_1, weights_2)
    output = simple_nn.calc_hidden_layer(input_x)
    assert (output == np.asarray([7, 15,22])).all() == True

def test_forward_input_2_6():
    weights_1 = np.asarray([[0.5, 1.5, 2], [1, 2, 3]])
    weights_2 = np.asarray([[1, 0.5], [1, 0.2], [2, 0.2]])
    input_x = np.asarray([2,6])
    simple_nn = SimpleNN(2, 3, 2, weights_1, weights_2)
    output = simple_nn.forward(input_x)
    assert (output == np.asarray([66, 10.9])).all() == True


if __name__=='__main__':
    pytest.main()